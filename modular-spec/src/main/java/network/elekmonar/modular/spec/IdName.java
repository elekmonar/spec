package network.elekmonar.modular.spec;

public interface IdName<T> {
	
	public T getId();
	
	public void setId(T id);
	
	public String getName();
	
	public void setName(String name);

}