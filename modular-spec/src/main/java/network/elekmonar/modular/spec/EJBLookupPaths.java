package network.elekmonar.modular.spec;

public interface EJBLookupPaths {
	 
	public static final String CLASSIFIER_IDENTIFIER_MANAGER = "java:jboss/exported/identifier-app/identifier-impl/ClassifierIdentifierManagerImpl!network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager";
	
	public static final String METAMODEL_IDENTIFIER_MANAGER = "java:jboss/exported/identifier-app/identifier-impl/MetamodelIdentifierManagerImpl!network.elekmonar.modular.spec.identifier.MetamodelIdentifierManager";
	
	public static final String METAMODEL_PROVIDER = "java:jboss/exported/metamodel-app/metamodel-ejb/MetamodelProviderImpl!network.elekmonar.modular.spec.metamodel.MetamodelProvider";
	
	//TODO check this uri
	public static final String INSTANCE_SET_PROVIDER = "java:jboss/exported/instance-set/InstanceSetProviderImpl!network.elekmonar.modular.spec.iset.InstanceSetProvider";
	
	public static final String CLASSIFIER_PROVIDER = "java:jboss/exported/classifier/ClassifierProviderImpl!network.elekmonar.modular.spec.classifier.ClassifierProvider";
	
	public static final String MEDIA_FILE_MANAGER = "java:jboss/exported/file-storage/MediaFileManagerImpl!network.elekmonar.file.storage.spec.MediaFileManager";

}