package network.elekmonar.modular.spec.identifier;

import java.util.List;

import javax.ejb.Remote;

/**
 * Интерфейс для управления идентификаторами
 * справочников, экземлпяров множеств и т.д.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Remote
public interface ClassifierIdentifierManager {

	/**
	 * Сгенерировать следующий идентификатор для справочника
	 * 
	 * @param resourceLabel Уникальная метка справочника (ресурса)
	 * @return Идентификатор
	 */
	public Long nextClassifierId(String resourceLabel);
	
	/**
	 * 
	 * @param resourceId
	 * @param originalId
	 * @return
	 */
	public Long getClassifierId(Short resourceId, Integer originalId);
	
	/**
	 * Сгенерировать следующий идентификатор для экземлпяра множества
	 * 
	 * @param resourceId Идентификатор ресурса
	 * @param workspaceId Идентификатор рабочего пространства
	 * @return Идентификатор множества
	 */
	public Long nextInstanceSetId(Short resourceId, Integer workspaceId);
	
	/**
	 * Получить список идентификаторов экземпляров множеств,
	 * привязанных к заданному рабочему пространству
	 * 
	 * @param workspaceId Идентификатор рабочего пространства
	 * @return Список идентификаторов
	 */
	public List<Long> getInstanceSetsIds(Integer workspaceId);
	
	/**
	 * Узнать идентификатор ресурса по заданному идентфикатору справочника
	 * 
	 * @param classifierId
	 * @return Числовое значение идентификатора ресурса
	 */
	public Short extractResourceId(Long classifierId);
	
}