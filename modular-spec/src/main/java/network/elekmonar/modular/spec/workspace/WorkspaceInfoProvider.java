package network.elekmonar.modular.spec.workspace;

import javax.ejb.Remote;

@Remote
public interface WorkspaceInfoProvider {
	
	WorkspaceInfo getWorkspaceInfo(String accessToken);

}