package network.elekmonar.modular.spec.metamodel;

import java.util.List;
import network.elekmonar.modular.spec.IdName;

import javax.ejb.Remote;

@Remote
public interface MetamodelProvider {
	
	public Short getResourceIdByLabel(String label);
	
	public String getResourceLabelById(Short id);
	
	public List<IdName<Integer>> getNamespaces();
	
	public List<IdName<Short>> getResources(Integer namespaceId);
	
	public IdName<Short> getResource(Short id);

}