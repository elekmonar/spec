package network.elekmonar.modular.spec.classifier;

import java.util.List;

import javax.ejb.Remote;

import network.elekmonar.modular.spec.IdName;

/**
 * Интерфейс, предоставляющий доступ к записям классификатора
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see IdName
 *
 */
@Remote
public interface ClassifierProvider {
	
	/**
	 * Выдать краткое представление (IdName) одной записи классификатора
	 * 
	 * @param id Идентификатор записи классификатора
	 * @param lang Язык, на котором необходимо получить запись
	 * @return представление IdName
	 */
	public IdName<Long> getSingleResult(Long id, String lang);

	/**
	 * 
	 * @param <T>
	 * @param beanClass
	 * @param id
	 * @return
	 */
	public <T> T getSingleResult(Class<T> beanClass, Long id);
	
	/**
	 * 
	 * @param <T>
	 * @param beanClass
	 * @param id
	 * @param language
	 * @return
	 */
	public <T> T getSingleResult(Class<T> beanClass, Long id, String language);
	
	/**
	 * 
	 * @param resourceId
	 * @param lookupText
	 * @param lang
	 * @return
	 */
	public List<IdName<Long>> getResultList(Short resourceId, String lookupText, String lang);
		
	/**
	 * 
	 * @param <T>
	 * @param beanClass
	 * @param ids
	 * @return
	 */
	public <T> List<T> getResultList(Class<T> beanClass, List<Long> ids);
	
	/**
	 * 
	 * @param <T>
	 * @param beanClass
	 * @param ids
	 * @param language
	 * @return
	 */
	public <T> List<T> getResultList(Class<T> beanClass, List<Long> ids, String language);
	
}