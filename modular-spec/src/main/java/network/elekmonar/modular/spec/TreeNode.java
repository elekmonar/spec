package network.elekmonar.modular.spec;

public interface TreeNode {
	
	TreeNode getParent();
	
	boolean isLeaf();
	
	boolean isRoot();

}