package network.elekmonar.modular.spec.workspace;

public interface WorkspaceInfo {
	
	public Integer getId();
	
	public String getName();
	
	public String getLang();

}