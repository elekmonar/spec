package network.elekmonar.modular.spec.iset;

import java.util.List;

import javax.ejb.Remote;

import network.elekmonar.modular.spec.IdName;

@Remote
public interface InstanceSetProvider {

	/**
	 * Получить список всех подмножеств, созданных в заданном рабочем пространстве
	 * 
	 * @param workspaceId
	 * @return
	 */
	List<IdName<Long>> getAll(Integer workspaceId);
	
	/**
	 * Получить информацию о заданном подмножестве
	 * 
	 * @param id
	 * @return
	 */
	IdName<Long> get(Long id);
	
	List<Long> getItems(Integer instanceSetId);
	
}