package network.elekmonar.modular.spec;

public interface IdNameFactory {
	
	<T> IdName<T> newInstance(Class<T> idType);

}